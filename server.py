#!/usr/bin/env python

import os

from flask import Flask
from flask_graphql import GraphQLView

from database import db
from schema import create_schema


app = Flask(__name__)
app.debug = True
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)

app.add_url_rule('/graphql', view_func=GraphQLView.as_view(
    'graphql', schema=create_schema(app), graphiql=True))

if __name__ == '__main__':
    app.run()
