GraphQL app for map-data-quality data access
============================================

Runing development server:

1. Clone repo
2. Create a file named `.env` where set `DATABASE_URL` variable to something like this:
   `mysql+pymysql://USER:PASSWORD@HOST/DBNAME`
3. Create virtualenv and install requirements:
   `virtualenv env; source env/bin/activate; pip install -r requirements.txt`
4. Run server like this:
   `flask run`
5. Open GraphiQL interface at http://localhost:5000/graphql
