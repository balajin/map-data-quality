from collections import OrderedDict
import os

import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType
import pymysql

from database import db, StatsRecord as RecordModel, Source as SourceModel


class SubclassableObjectType(graphene.ObjectType):
    """
    Helper class. Allows its subclasses
    to easily be further subclassed in runtime.
    This is achieved by calling a :meth:`subclass` method
    passing it the desired subclass name and some optional arguments.
    These arguments will be passed to the internal :meth:`_get_fields` method
    which will determine the shape of newly created class
    by yielding (name, field) tuples.

    When implementing a subclass:
    - regular method will be called on *instance of subcalss*
      of the class you implement;
    - class method will be called on *subclass* or *the class* you implement.
    """
    @classmethod
    def subclass(cls, name, *args, **kwargs):
        description = kwargs.pop('description', None)
        fields = OrderedDict(cls._get_fields(*args, **kwargs))
        return type(name, (cls,), fields, description=description)

    @classmethod
    def _get_fields(cls, *args, **kwargs):
        """
        Should either return an OrderedDict (or dict)
        or yield tuples of (name, field)
        """
        raise NotImplementedError


class DetailsBase(SubclassableObjectType):
    """
    Represents a container for various `DetailsType`s.
    This class itself should not be used directly,
    instead it should be :meth:`subclass`ed with a path to SQL files directory.
    Each of the resulting class' fields is backed by an SQL file.

    Only one subclass of this class should exist in the schema.
    """
    def __init__(self, fi_id, date):
        self.fi_id = fi_id
        self.date = date

    def resolve(self, info):
        """
        Generic resolve method. Not called directly by graphene;
        instead, we pass it to Fields as `resolver`.
        It determines the actual field name from the info object
        and then calls that field type's :meth:`load` method.
        """
        field = self._meta.fields[info.field_name]
        # field.type is List;
        # field.type.of_type is a subclass of DetailsType
        return field.type.of_type.load(info, self.fi_id, self.date)

    @classmethod
    def _get_fields(cls, sql_path):
        for fname in os.listdir(sql_path):
            if not fname.endswith('.sql'):
                continue
            name = fname[:-4]
            fpath = os.path.join(sql_path, fname)
            with open(fpath, 'r') as f:
                sql = f.read()
            yield name, graphene.Field(
                graphene.List(
                    DetailsType.subclass(name, sql),
                ),
                resolver=cls.resolve,
            )


class DetailsType(SubclassableObjectType):
    """
    Subclasses of this class represent result of executing an SQL query.
    During subclassing, query itself is parsed for returning cols and types
    and corresponding fields are created on the subclass.

    Don't instantiate directly!
    First create sublcass using .subclass()
    then instantiate that sublass using .load()

    Many subclasses of this class will exist in the schema,
    one for every SQL file.
    They are actually created by DetailsBase class.
    """

    _sql = None

    @classmethod
    def _get_fields(cls, sql):
        # Implementation details:
        # We create fields but don't create any resolvers for them
        # because all our fields will be resolved in load() classmethod
        # when instantiating the class.

        # First store our SQL query (as subcls._sql)
        yield '_sql', sql

        # In order to parse the query, we execute it with dummy parameters
        rp = cls.execute_raw(sql, 0, None)
        # And then analyze cursor's description
        for col in rp.cursor.description:
            name = col[0]
            if not name[:1].isalpha():
                raise ValueError(
                    'SQL file {sqlf}.sql: '
                    'one of the fields seemingly has no name '
                    'or incorrect name "{name}". '
                    'This is not allowed.'.format(sqlf=classname, name=name)
                )

            raw_type = col[1]  # dbapi type
            field_type = cls._mysql_to_graphene(raw_type)

            yield name, graphene.Field(field_type)
        rp.close()

    @staticmethod
    def _mysql_to_graphene(typ):
        if typ in pymysql.STRING or typ == pymysql.FIELD_TYPE.NULL:
            return graphene.String
        elif typ in pymysql.BINARY:
            # XXX which would be the better option?
            return graphene.String
        elif typ in pymysql.NUMBER:
            if typ in (
                pymysql.FIELD_TYPE.DECIMAL,
                pymysql.FIELD_TYPE.DOUBLE,
                pymysql.FIELD_TYPE.FLOAT,
            ):
                return graphene.Float
            else:
                return graphene.Int
        elif typ in pymysql.DATE:
            return graphene.Date
        elif typ in pymysql.TIME:
            return graphene.Time
        elif typ in pymysql.DATETIME:
            return graphene.DateTime
        else:
            raise ValueError('Unknown db type %s' % raw_type)

    @staticmethod
    def execute_raw(sql, fi_id, date):
        return db.session.execute(
            sql, dict(
                fi_id=fi_id,
                date=date,
            ),
        )

    # to be used as constructor of the actual details type
    @classmethod
    def load(cls, info, fi_id, date):
        if not cls._sql:
            raise TypeError('Cannot load generic DetailsType')

        rp = cls.execute_raw(cls._sql, fi_id, date)
        for row in rp:
            fields = dict(zip(rp.keys(), row))
            yield cls(**fields)
        rp.close()


class StatsBase(SubclassableObjectType):
    """
    Only one subclass of this one should exist in the schema.
    """
    @classmethod
    def _get_fields(cls, names):
        types = {n: None for n in names}
        for r in RecordModel.query.all():
            if not isinstance(r.stats, dict):
                print('WARNING: not a dict', r.stats)
                continue
            for key, val in r.stats.items():
                if key not in types:
                    # This really-present stats field
                    # is not backed by the query
                    # so we will ignore it
                    continue
                if types[key] and types[key] != type(val):
                    # WARNING: different types
                    # string is more common type than int so prefer it
                    types[key] = str
                    continue
                types[key] = type(val)

        # now that we collected all info, do yield final field types
        for key, typ in sorted(types.items()):
            if typ is None:
                print('WARNING: field %s does not present in real stat records' %
                      key)
                typ = str
            field = {
                str: graphene.String,
                int: graphene.Int,
            }[typ]
            yield (key, field())


def create_schema(app):
    with app.app_context():
        return create_schema_in_app_context()


def create_schema_in_app_context():
    # Create deails collection from SQL files
    Details = DetailsBase.subclass('Details', os.path.join(
        os.path.dirname(__file__), 'sqls'))

    # For each detail filename,
    # determine which column type should it use in stats.
    # (We ignore stats records not backed by sql details files)
    Stats = StatsBase.subclass(
        'Stats',
        Details._meta.fields.keys(),
        description='Single statistics record',
    )

    class Record(SQLAlchemyObjectType):
        class Meta:
            model = RecordModel
            # Do fetch it from db but don't present it in schema
            # as we will instead provide a prettified `stats` field
            exclude_fields = ('stats_object',)

        # For some reason primary key fields are represented as strings
        # even while they are integers in db model (and in db itself).
        # So let's explicitly declare them as integer.
        source_id = graphene.Field(graphene.Int)
        fi_id = graphene.Field(graphene.Int)
        sequence_id = graphene.Field(graphene.Int)

        stats = graphene.Field(Stats)

        def resolve_stats(self, info):
            # from the database, self.stats is a json object (hopefully)
            # so we just wrap it in our Stats class
            return Stats(**{
                k: v for k, v in self.stats.items()
                if k in Stats._meta.fields
            })

    SourceType = graphene.Enum('SourceType', [
        (s.source, s.source_id)
        # XXX what if SourceModel content changes?
        # For now you should restart the server
        # to account for these changes.
        for s in SourceModel.query
    ])

    source_arg = graphene.Argument(
        SourceType,
        name='source',
        # default should be a string representation of enum's value
        default_value=SourceType.get(7).name,
    )

    class Query(graphene.ObjectType):
        all_stats = graphene.List(
            Record,
            source=source_arg,
            latest_only=graphene.Boolean(),
        )

        def resolve_all_stats(self, info, source, latest_only=False):
            q = Record.get_query(info).filter_by(source_id=source)
            if latest_only:
                # XXX what do we want to group on?
                # Not just FI_ID, there are also file_type and some other stuff
                raise NotImplementedError
                q = q.order_by(RecordModel.date_loaded.desc())
            return q.all()

        stats = graphene.Field(Record, dict(
            source=source_arg,
            fi_id=graphene.NonNull(graphene.Int),
        ))

        def resolve_stats(self, info, source, fi_id):
            return Record.get_query(info).filter_by(
                source_id=source,
                fi_id=fi_id,
            ).order_by(RecordModel.date_loaded.desc()).first()

        details = graphene.Field(Details, dict(
            fi_id=graphene.NonNull(graphene.Int),
            date=graphene.NonNull(graphene.Date),
        ))

        def resolve_details(self, info, fi_id, date):
            return Details(fi_id, date)

    return graphene.Schema(query=Query)
