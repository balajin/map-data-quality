from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Source(db.Model):
    __tablename__ = 'sources'

    source_id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.String(100), nullable=False)


class StatsRecord(db.Model):
    __tablename__ = 'stats'

    source = db.Column(db.String(100), nullable=False)
    source_id = db.Column(db.Integer, nullable=False, primary_key=True)
    fi_id = db.Column(db.Integer, nullable=False, primary_key=True)
    file_type = db.Column(db.String(100), nullable=False, primary_key=True)
    sequence_id = db.Column(db.Integer, nullable=False, primary_key=True)
    count = db.Column(db.Integer, nullable=False)
    stats_object = db.Column(db.JSON)
    date_loaded = db.Column(db.Date, nullable=False, primary_key=True)
    time_inserted = db.Column(db.DateTime, nullable=False) # TODO default now

    @property
    def stats(self):
        # Fixup incorrect data in the DB (string '{}' instead of empty object)
        if self.stats_object == '{}':
            # TODO db.sesion.add(self) and commit later?
            return {}
        return self.stats_object
